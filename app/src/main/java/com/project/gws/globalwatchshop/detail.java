package com.project.gws.globalwatchshop;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class detail extends AppCompatActivity {

    Database database;

    Cursor cur;

    TextView txtnama, txtharga, txtpid, desk;
    FloatingActionButton fabbuy;
    ImageView imgproduk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toast.makeText(detail.this, "Load database to detail...", Toast.LENGTH_SHORT).show();
        database = new Database(this);
        Toast.makeText(detail.this, "Fetching data...", Toast.LENGTH_SHORT).show();
        cur = database.ambildata(product.selected);

        txtnama = (TextView) findViewById(R.id.txtnama);
        txtharga = (TextView) findViewById(R.id.txtharga);
        txtpid = (TextView) findViewById(R.id.txtpid);
        desk = (TextView) findViewById(R.id.desk);
        fabbuy = (FloatingActionButton) findViewById((R.id.fabbuy));
        imgproduk = (ImageView) findViewById(R.id.imgproduk);
        cur.moveToPosition(0);

        txtnama.setText(cur.getString(1));
        txtharga.setText("Rp. " + cur.getString(2));
        txtpid.setText("Product ID : " + cur.getString(0));
        desk.setText(cur.getString(3));
        desk.setMovementMethod(new ScrollingMovementMethod());


    }

    public void openimage(View view) {
        Intent intent = new Intent(getApplicationContext(), picture.class);
        startActivity(intent);
    }

    public void insertdata(View view) {
        database.insertdata(txtpid.getText().toString());
        Toast.makeText(detail.this, "Data inserted", Toast.LENGTH_SHORT).show();
    }
}
