package com.project.gws.globalwatchshop;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Moses on 12/5/2016.
 */

public class CustomAdapterPromo extends BaseAdapter{

    ArrayList<String> proid;
    ArrayList<String> img;
    ArrayList<String> link;
    promo promo;
    Context c;
    LayoutInflater layoutinflater = null;

    public CustomAdapterPromo(ArrayList<String> proid, ArrayList<String> img,ArrayList<String> link, com.project.gws.globalwatchshop.promo promo, Context c) {
        this.proid = proid;
        this.img = img;
        this.promo = promo;
        this.c = c;
        this.layoutinflater = (LayoutInflater)promo.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return proid.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return 1;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View Rowview = layoutinflater.inflate(R.layout.custompromo,null);

        String nama = img.get(i);

        int image = c.getResources().getIdentifier(nama,"drawable",c.getPackageName());
        ImageView imgpromo = (ImageView)Rowview.findViewById(R.id.imgpromo);
        imgpromo.setImageResource(image);



        return Rowview;
    }
}
