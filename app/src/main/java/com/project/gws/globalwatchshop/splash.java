package com.project.gws.globalwatchshop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.IOException;

public class splash extends AppCompatActivity {

    Database database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        database = new Database(this);

        Thread myThread = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(3000);
                    Intent intent = new Intent(getApplicationContext(),promo.class);
                    startActivity(intent);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();

        try{
            Toast.makeText(splash.this, "Loading Database...", Toast.LENGTH_SHORT).show();
            database.createDB();
            Toast.makeText(splash.this, "Database Loaded", Toast.LENGTH_SHORT).show();
        } catch (IOException ioe){
            Toast.makeText(splash.this, "Gagal membuat database", Toast.LENGTH_SHORT).show();
            throw new Error("Unable to create database");
        }
    }
}
