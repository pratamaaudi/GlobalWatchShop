package com.project.gws.globalwatchshop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Moses on 12/5/2016.
 */

public class CustomAdapter extends BaseAdapter{
    ArrayList<String> pid;
    ArrayList<String> nama;
    ArrayList<String> harga;
    ArrayList<String> img;
    product product;
    Context c;
    LayoutInflater layoutinflater = null;

    public CustomAdapter(ArrayList<String> pid, ArrayList<String> nama, ArrayList<String> harga, ArrayList<String> img, com.project.gws.globalwatchshop.product product, Context c) {
        this.pid = pid;
        this.nama = nama;
        this.harga = harga;
        this.product = product;
        this.img = img;
        this.c = c;
        this.layoutinflater = (LayoutInflater)product.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pid.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View Rowview = layoutinflater.inflate(R.layout.customlist,null);

        TextView txtnama = (TextView)Rowview.findViewById(R.id.txtnama);
        txtnama.setText(nama.get(i));

        TextView txtharga = (TextView)Rowview.findViewById(R.id.txtharga);
        txtharga.setText("Rp. "+harga.get(i));

        TextView txtpid = (TextView)Rowview.findViewById(R.id.txtpid);
        txtpid.setText("Product ID : "+String.valueOf(pid.get(i)));

        String nama = img.get(i);

        int image = c.getResources().getIdentifier(nama,"drawable",c.getPackageName());
        ImageView imgprod = (ImageView)Rowview.findViewById(R.id.imgproduk);
        imgprod.setImageResource(image);
        return Rowview;
    }
}
