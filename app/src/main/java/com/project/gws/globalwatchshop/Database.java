package com.project.gws.globalwatchshop;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Moses on 12/5/2016.
 */

public class Database extends SQLiteOpenHelper {
    private static String DB_PATH = "/data/data/com.project.gws.globalwatchshop/databases/";
    private static String DB_NAME = "Jam.db";
    private SQLiteDatabase database;
    private final Context myContext;

    public static final String NAMA_TABEL = "katalog";
    public static final String KOL_1 = "id";
    public static final String KOL_2 = "nama";
    public static final String KOL_3 = "harga";
    public static final String KOL_4 = "deskripsi";
    public static final String KOL_5 = "img";

    public static final String NAMA_TABEL2 = "keranjang";
    public static final String KOL2_1 = "id";
    public static final String KOL2_2 = "produk";

    public static final String NAMA_TABEL3 = "promo";
    public static final String KOL3_1 = "id";
    public static final String KOL3_2 = "img";
    public static final String KOL4_2 = "link";

    public Database(Context context) {
        super(context, DB_NAME, null, 1);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void createDB() throws IOException {
        boolean adaDB = checkDB();

        if (adaDB) {

        } else {
            this.getReadableDatabase();
            try {
                copyDB();
            } catch (IOException e) {
                throw new Error("error bro");
            }
        }
    }

    private void copyDB() throws IOException {
        InputStream myInput = myContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private boolean checkDB() {
        SQLiteDatabase cek = null;
        try {
            String myPATH = DB_PATH + DB_NAME;
            cek = SQLiteDatabase.openDatabase(myPATH, null, SQLiteDatabase.OPEN_READONLY);
        } catch (SQLException e) {

        }
        if (cek != null) {
            cek.close();
        }
        return cek != null ? true : false;
    }

    public Cursor ambilsemuadata() {
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT * FROM " + NAMA_TABEL, null);
        return cur;
    }

    public Cursor ambilsemuapromo() {
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT * FROM " + NAMA_TABEL3, null);
        return cur;
    }

    public Cursor ambildata(String id) {
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cur = database.rawQuery("SELECT * FROM " + NAMA_TABEL+" WHERE id = "+id, null);
        return cur;
    }

    public boolean insertdata(String id) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues inputan = new ContentValues();
        inputan.put(KOL2_2, id);
        long hasil = database.insert(NAMA_TABEL2, null, inputan);
        if (hasil == -1) {
            return false;
        } else {
            return true;
        }
    }
}


