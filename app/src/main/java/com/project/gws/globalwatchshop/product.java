package com.project.gws.globalwatchshop;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.net.URL;
import java.util.ArrayList;

public class product extends AppCompatActivity {

    private ArrayList<String> pid = new ArrayList<>();
    private ArrayList<String> nama = new ArrayList<>();
    private ArrayList<String> harga = new ArrayList<>();
    private ArrayList<String> img = new ArrayList<>();

    ListView listproduk;

    Button btncart;

    Database database;

    Cursor cur;

    public static String selected = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        Toast.makeText(product.this, "Load database to product...", Toast.LENGTH_SHORT).show();
        database = new Database(this);

        Toast.makeText(product.this, "Fetching database data to cursor...", Toast.LENGTH_SHORT).show();
        cur = database.ambilsemuadata();
        Toast.makeText(product.this, "Fetching database complete", Toast.LENGTH_SHORT).show();

        Toast.makeText(product.this, "Checking cursor data....", Toast.LENGTH_SHORT).show();
        if (cur.getCount() == 0) {
            Toast.makeText(product.this, "No data", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(product.this, "Data found, processing data...", Toast.LENGTH_LONG).show();
            while (cur.moveToNext()) {
                pid.add(cur.getString(0));
                nama.add(cur.getString(1));
                harga.add(cur.getString(2));
                img.add(cur.getString(4));
            }
        }
        Toast.makeText(product.this, "Loading CustomAdapter....", Toast.LENGTH_LONG).show();
        listproduk = (ListView) findViewById(R.id.listproduk);
        listproduk.setAdapter(new CustomAdapter(pid, nama, harga,img, this, this));

        listproduk.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                cur.moveToPosition(i);
                selected = cur.getString(0);
                Toast.makeText(product.this, "Product ID : " + selected, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), detail.class);
                startActivity(intent);
            }
        });

        btncart = (Button)findViewById(R.id.btncart);

    }
    public void openweb(View view){
        Uri uri = Uri.parse("http://www.google.com");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

}
