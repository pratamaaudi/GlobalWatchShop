package com.project.gws.globalwatchshop;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class promo extends AppCompatActivity {

    Database database = new Database(this);

    Cursor cur;

    ListView promo;

    private ArrayList<String> proid = new ArrayList<>();
    private ArrayList<String> img = new ArrayList<>();
    private ArrayList<String> link = new ArrayList<>();
//    private int[] imagelist = new int[R.drawable.icon];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promo);

        cur = database.ambilsemuapromo();

        if (cur.getCount() == 0) {
            Toast.makeText(promo.this, "No data", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(promo.this, "Data found, processing data...", Toast.LENGTH_SHORT).show();
            while (cur.moveToNext()) {
                proid.add(cur.getString(0));
                img.add(cur.getString(1));
                link.add(cur.getString(2));
            }
        }

        promo = (ListView) findViewById(R.id.list);
        promo.setAdapter(new CustomAdapterPromo(proid, img, link, this, this));

        promo.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                cur.moveToPosition(i);
                String url = cur.getString(2);
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }

    public void shop(View view){
        Intent intent = new Intent(getApplicationContext(),product.class);
        startActivity(intent);
    }
}
